package com.example.myapplication;

import androidx.appcompat.app.AppCompatActivity;

import android.media.VolumeShaper;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private static final int FIRST_NUMBER = 9;
    private static final int SECOND_NUMBER = 5;
    TextView resultTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        resultTextView = findViewById(R.id.txTvResult);
        findViewById(R.id.minus).setOnClickListener(V -> minusOperation());
        findViewById(R.id.plus).setOnClickListener(V -> plusOperation());
        findViewById(R.id.multiply).setOnClickListener(V -> multiOperation());
        findViewById(R.id.division).setOnClickListener(V -> divOperation());
    }

    private void plusOperation() {
        resultTextView.setText(plusResult());
    }

    private void minusOperation() { resultTextView.setText(minusResult()); }

    private void multiOperation() { resultTextView.setText(multiResult());}

    private void divOperation() { resultTextView.setText(divResult());}

    private String minusResult() {
        return String.valueOf(FIRST_NUMBER - SECOND_NUMBER);
    }

    private String plusResult() {
        return String.valueOf(FIRST_NUMBER + SECOND_NUMBER);
    }

    private String multiResult() { return String.valueOf(FIRST_NUMBER * SECOND_NUMBER); }

    private String divResult() { return String.valueOf(FIRST_NUMBER / SECOND_NUMBER); }
}